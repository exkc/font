
install:
	rm -rf /usr/share/fonts/exkc/
	mkdir -p /usr/share/fonts/exkc/
	cp -rf ./zh /usr/share/fonts/exkc/
	cp -rf ./en /usr/share/fonts/exkc/
	cp -rf ./emoji /usr/share/fonts/exkc/
	fc-cache -sfv
.PHONY:  install 

